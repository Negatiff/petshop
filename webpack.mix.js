let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
  'public/assets/js/jquery.min.js',
  'public/media/js/jquery-3.2.1.min.js',
  'public/media/js/slick.min.js',
  'public/media/js/icheck.min.js',
  'public/media/js/remodal.min.js',
  'public/media/js/jquery.selectric.js',
  'public/media/js/baron.min.js',
  'public/media/js/toastr.min.js',
  'public/media/js/jquery.sticky.js',
  'public/media/js/jquery.validate.js',
  'public/media/js/jquery.maskedinput.js',
  'public/media/js/custom.js'
  ],'public/build/build.js');

mix.styles([
    'public/media/css/main.css'
], 'public/media/css/build.css');
