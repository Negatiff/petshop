@extends('layouts.app')
@section('title', 'Заказ успешно оформлен' )
@section('content')

  <div class="page-content">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="javascript:void(0)">Главная</a>
          </li>
          <li>
            <a href="/">Корзина</a>
          </li>
          <li>
            Заказ оформлен
          </li>
        </ul>
      </div>
      <div class="cart-empty" style="width: 100%;">
        <div class="cart-empty-right" style="padding-left: 0;">
          <div class="cart-empty-title">Заказ успешно оформлен</div>
          <div class="cart-empty-text custom-cart-text"><strong>Номер вашего заказа <span class="green">{{$order_id}}</span></strong></div>
          <div class="cart-empty-text">Наш оператор свяжется с вами для подтверждения заказа в рабочее время с 10:00 до 19:00 по МСК</div>
        </div>
      </div>
    </div>
    <br>
    <br>
    <br>
  </div>

@endsection