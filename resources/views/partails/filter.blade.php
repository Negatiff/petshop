
        <div class="filters">
            <form method="GET" class="filters-list">
              @foreach ($filters as $filter)
                @if ($filter->attribute->filter == 1)
                  <div class="filters-item">
                    <div class="filters-title">{{$filter->attribute->title}}</div>
                    <div class="filters-content">
                      @foreach ($filter->attribute->attribute_val as $val)
                      <div class="filters-content-item">
                        <label for="filter-{{$val->attribute_value}}"><input name="{{$filter->attribute->id}}" type="radio" class="radio-field" id="filter-{{$val->attribute_value}}" value="{{$val->id}}">{{$val->attribute_value}}</label>
                      </div>
                      @endforeach
                    </div>
                  </div>
                @endif
              @endforeach
              <input type="submit" value="Применить фильтры" class="bottom-form-item-button">
            </form>
        </div>
          <br>
          <br>
          <br>