@extends('layouts.app')
@section('title', $cat->title )
@section('content')

  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
            <a href="/products">Корма</a>
          </li>
          <li>
            {{$cat->title}}
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="category">
    <div class="container category-content">
      <div class="category-right" style="width: 100%;">
        <div class="category-title title">{{count($products)}} товаров</div>
          <div class="catalog-list catalog-list-category">

            @if (count($products))
              @foreach ($products as $product)
                @include('partails.card', ['product' => $product])
              @endforeach
            @else
              <p>В данной категории нет товаров</p>
            @endif

          </div>
          <div class="more" style="display: none;">
            <div class="more-title">Показать еще</div>
            <div class="more-icon">
              <div class="icon icon-arrow-down"></div>
            </div>
          </div>
          <br>
          <br>
          <br>
      </div>
    </div>
  </div>
  <div id="ingridients"></div>
  <div class="adventages">
    <div class="container">
      <div class="adventages-title title">Ингредиенты</div>
      <div class="adventages-list">
        <a href="/ingredienty_kotorye_my_ispolzuem" class="adventages-item" style="width: 45%;">
          <div class="adventages-item-icon"><img src="/media/img/ing-1.jpg" alt=""></div>
          <div class="adventages-item-title">Ингредиенты, которые мы используем</div>
        </a>
        <a href="/ingredienty_kotorye_my_ne_ispolzuem" class="adventages-item" style="width: 45%;">
          <div class="adventages-item-icon"><img src="/media/img/ing-2.jpg" alt=""></div>
          <div class="adventages-item-title">Ингредиенты, которые мы не используем</div>
        </a>
      </div>
    </div>
  </div>

@endsection