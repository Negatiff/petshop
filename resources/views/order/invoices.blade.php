<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- saved from url=(0114)https://petdiets.ru/admin/index.php?route=sale/order/invoice&token=db2c778af772c6d2c5bd9636886f06a2&order_id=11178 -->
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="ru" xml:lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Счета</title>
<!--<base href="https://petdiets.ru/admin/">--><base href=".">
<link rel="stylesheet" type="text/css" href="/css/invoice.css">
</head>
<body>
@php
  Cart::destroy();
@endphp
@foreach ($orders as $order)
<div style="page-break-after: always;">
  <h1>Счет заказа #{{$order->order_id}}</h1>
  <table class="store">
    <tbody><tr>
      <td>Petdiets.RU - Корм для вашего питомца!<br>
        Адрес<br>
        Телефон: +7 (926) 865-36-63<br>
                info@petdiets.ru<br>
        https://petdiets.ru</td>
      <td align="right" valign="top"><table>
          <tbody><tr>
            <td><b>Дата добавления:</b></td>
            <td>{{$order->created_at->format('d.m.Y')}}</td>
          </tr>
                    <tr>
            <td><b>№ заказа:</b></td>
            <td>{{$order->order_id}}</td>
          </tr>
          <tr>
            <td><b>Способ доставки:</b></td>
            <td>{{$order->delivery_type}}</td>
          </tr>
                  </tbody></table></td>
    </tr>
  </tbody></table>
  <table class="address">
    <tbody><tr class="heading">
      <td width="50%"><b>Адрес оплаты</b></td>
      <td width="50%"><b>Адрес доставки</b></td>
    </tr>
    <tr>
      <td><strong>{{$order->users->name}}</strong><br>{{$order->address}}<br>
        {{$order->email}}<br> {{$order->phone}}
      </td>
      <td><strong>{{$order->users->name}}</strong><br>{{$order->address}}<br>
      </td>
    </tr>
  </tbody></table>
  <table class="product">
    <tbody><tr class="heading">
      <td><b>Товар</b></td>
      <td><b>Модель</b></td>
      <td align="right"><b>Количество</b></td>
      <td align="right"><b>Цена за единицу</b></td>
      <td align="right"><b>Итого</b></td>
    </tr>
      @php
        Cart::restore($order->order_id);
      @endphp
      @foreach (Cart::content() as $row)
      <tr>
        <td>{{$row->name}}</td>
        <td align="right">@if (App\Product::where('id',$row->id)->first()->category_id == 17) <font size="12"><strong>X</strong></font> @endif</td>
        <td align="right">{{$row->qty}}</td>
        <td align="right">{{$row->price}} р.</td>
        <td align="right">{{$row->price * $row->qty}} р.</td>
      </tr>
      @endforeach
      <tr>
        <td align="right" colspan="4"><b>Сумма:</b></td>
        <td align="right">{{$order->total}} р.</td>
      </tr>
      <tr>
      <td align="right" colspan="4"><b>{{$order->delivery_type}}</b></td>
        <td align="right">{{$order->delivery_price}} р.</td>
      </tr>
      @if ($order->order_sale > 0)
      <tr>
      <td align="right" colspan="4"><b>Скидка</b></td>
        <td align="right">{{$order->total * $order->order_sale / 100}} р.</td>
      </tr>
      @endif
      <td align="right" colspan="4"><b>Итого:</b></td>
        <td align="right">{{$order->total_price}} р.</td>
      </tr>
      </tbody></table>
      <table class="product">
      </table>
  </div>
  @php
  Cart::destroy();
  @endphp
@endforeach
</body></html>