<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- saved from url=(0114)https://petdiets.ru/admin/index.php?route=sale/order/invoice&token=db2c778af772c6d2c5bd9636886f06a2&order_id=11178 -->
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="ru" xml:lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Счета</title>
<!--<base href="https://petdiets.ru/admin/">--><base href=".">
<link rel="stylesheet" type="text/css" href="/css/invoice.css">
</head>
<body>
@php
  Cart::destroy();
@endphp
<div style="page-break-after: always;">
  <h1>Сводный лист</h1>
  <table>
    <tbody><tr>
      <td><b>Наименование</b></td>
      <td style="padding-left:20px;"><b>Количество</b></td>
    </tr>
    @php
      $total = 0;
    @endphp
    @foreach ($orders as $order)
        @php
          Cart::restore($order->order_id);
        @endphp
        @foreach (Cart::content() as $row)
        <tr>
          <td>{{$row->name}}</td>
          <td style="text-align: left;padding-left: 20px;">{{$row->qty}}</td>
        </tr>
        @endforeach
        @php
        $total = $total + Cart::count();
        Cart::destroy();
        @endphp
    @endforeach
      <tr>
        <td style="font-weight: bold;">Итого</td>
        <td style="text-align: left;font-weight: bold;padding-left: 20px;">{{$total}}</td>
      </tr>
      </tbody></table>
      <table class="product">
      </table>
  </div>

</body></html>