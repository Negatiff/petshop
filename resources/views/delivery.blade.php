@extends('layouts.app')
@section('title', 'Доставка' )
@section('content')
  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="page-content-title title">Доставка</div>
      <div class="page-content-about about about-delivery">
        <div class="container">
          <div class="about-list">
            <div class="about-item">
              <div class="about-icon"><div class="icon icon-location"></div></div>
              <div class="about-title">Карта<br><span>по московскому региону</span></div>
            </div>
            <div class="about-item">
              <div class="about-icon"><div class="icon icon-time"></div></div>
              <div class="about-title">Время доставки <br> <span>ежедневно, при заказе с пн-сб с 11 до 22 часов</span></div>
            </div>
            <div class="about-item">
              <div class="about-icon"><div class="icon icon-list"></div></div>
              <div class="about-title">Прайс-лист <br> <span>на доставку по Московскому региону</span></div>
            </div>
          </div>
        </div>
      </div>
      <div class="page-content-body page-article-body">
        <p>Доставка автотранспортом осуществляется согласно утвержденному <a href="javascript:void(0)">прайсу</a>. <br> Для более точного расчета, Вам всегда помогут наши консультанты по телефону: <strong>+7 (925) 100-52-07</strong></p>
        <div class="map">
          <img src="/media/img/map.jpg" alt="">
        </div>
      </div>
  <div class="catalog">
    <div class="container">
      <div class="catalog-tabs">
        <div class="catalog-tabs-nav">
          <div class="catalog-tabs-nav-title title">Возможно вас заинтересуют эти товары</div>
        </div>
        <div class="catalog-arrows">
          <div class="catalog-arrow catalog-arrow-left">
            <div class="icon icon-arrow-l"></div>
          </div>
          <div class="catalog-arrow catalog-arrow-right">
            <div class="icon icon-arrow-r"></div>
          </div>
        </div>
        <div class="catalog-tabs-list">
          <div class="catalog-tabs-item catalog-tabs-item--1 active">
            <div class="catalog-list catalog-list-4">
            @foreach ($popular as $product)
              <div class="catalog-item">
                <div class="catalog-stars">
                  <div class="icon icon-star catalog-star catalog-star--active"></div>
                  <div class="icon icon-star catalog-star catalog-star--active"></div>
                  <div class="icon icon-star catalog-star catalog-star--active"></div>
                  <div class="icon icon-star catalog-star catalog-star--active"></div>
                  <div class="icon icon-star catalog-star catalog-star--active"></div>
                </div>
                <div class="catalog-labels">
                </div>
                <div class="catalog-image">
                  <a href="/product/{{$product->id}}"><img src="/storage/{{ json_decode($product->images)[0]}}" alt=""></a>
                </div>
                <div class="catalog-title">
                  <a href="/product/{{$product->id}}">{{$product->title}}</a>
                </div>
                <div class="catalog-price">
                  {{$product->price}} <span>й</span>
                </div>
                <div class="catalog-button">
                  <a href="javascript:void(0)"><span>В корзину</span><i class="icon icon-plus"></i></a>
                </div>
              </div>
            @endforeach
            </div>
            <br>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
    </div>
  </div>

@endsection