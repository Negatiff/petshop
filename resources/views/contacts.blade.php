@extends('layouts.app')
@section('title', 'Контакты')
@section('content')

  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
          Контакты
          </li>
        </ul>
      </div>

      <div class="page-content-title title">Контакты</div>
      <div class="page-content-about about">
        <div class="container">
          <div class="page-content-body page-article-body">
            <p>Уважаемые клиенты!</p>
            <p>Если у вас возникли какие-либо вопросы, вы можете связаться с нами по указанным ниже контактам для их разрешения.</p>
            <p>8 499 398 36 30 мы принимаем Ваши звонки до 19.00</p>
            <p>info@petdiets.ru</p>
            <p>Рабочие дни: понедельник — суббота </p>
            <p>Воскресенье — выходной. В этот день Вы можете оформить заказ на сайте.</p>
            <p>Адрес: г. Москва, Мичуринский проспект, ул. Олимпийская деревня 1к1</p>
            <p>Реквизиты: ОГРН 1157746126724, ИНН 7729447311, КПП 772901001"</p>
          </div> 
          <br>
          <br>
          <br>
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Afc8ed5c75b135c4bb36d559ba134ffff053c3b9bc5594e8b5fff8bf7c8cb236a&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
@endsection