@extends('layouts.app')
@section('title', 'Корма' )
@section('content')
  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
            Корма
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="catalog">
    <div class="container catalog-row">
      <div class="catalog-info">
        <div class="catalog-info-icon">
          <a href="javasctip:void(0)"><div class="icon icon-homa"></div></a>
        </div>
        <div class="catalog-info-title"><a href="javasctip:void(0)">Сырые рационы</a></div>
      </div>
      <div class="catalog-list catalog-list-3">
        @foreach ($meat as $product)
          @include('partails.card', ['product' => $product])
        @endforeach      </div>
    </div>
  </div>
  <div class="catalog">
    <div class="container catalog-row">
      <div class="catalog-info">
        <div class="catalog-info-icon">
          <a href="javasctip:void(0)"><div class="icon icon-cat"></div></a>
        </div>
        <div class="catalog-info-title"><a href="javasctip:void(0)">Обработанные корма</a></div>
      </div>
      <div class="catalog-list catalog-list-3">
        @foreach ($term as $product)
          @include('partails.card', ['product' => $product])
        @endforeach
      </div>
    </div>
  </div>
  <div class="catalog">
    <div class="container catalog-row">
      <div class="catalog-info">
        <div class="catalog-info-icon">
          <a href="javasctip:void(0)"><div class="icon icon-dog"></div></a>
        </div>
        <div class="catalog-info-title"><a href="javasctip:void(0)">Лакомства</a></div>
      </div>
      <div class="catalog-list catalog-list-3">
        @foreach ($lac as $product)
          @include('partails.card', ['product' => $product])
        @endforeach
      </div>
    </div>
  </div>

@endsection