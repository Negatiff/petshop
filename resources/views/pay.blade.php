@extends('layouts.app')
@section('title', 'Доставка' )
@section('content')
  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
          Оплата
          </li>
        </ul>
      </div>
      <div class="page-content-title title">Оплата</div>
      <div class="page-content-subtitle">Возможны следующие варианты оплаты:</div>
      <div class="page-content-about about about-pay">
        <div class="container">
          <div class="about-list">
            <div class="about-item">
              <div class="about-icon"><div class="icon icon-cash"></div></div>
              <div class="about-title">Наличный платеж<br><span>Вы оплачиваете заказ при получении</span></div>
            </div>
            <div class="about-item">
              <div class="about-icon"><div class="icon icon-card"></div></div>
              <div class="about-title">Банковская карта <br> <span>Оплата банковскими картами (VISA, MASTERCARD, <br> MAESTRO)</span></div>
            </div>
            <div class="about-item">
              <div class="about-icon"><div class="icon icon-eceommerce"></div></div>
              <div class="about-title">Электронные деньги <br> <span>WebMoney, Qiwi, ЯндексДеньги, АльфаБанк</span></div>
            </div>
          </div>
        </div>
      </div>
      <div class="page-content-body page-article-body">
        <h3 class="pay-title">Уважаемые клиенты! <span class="danger">Важно!</span></h3>
        <p>Оплачивайте заказ только после подтверждения оператором или менеджером магазина по телефону или электронной почте.</p>
      </div>
      <div class="catalog">
        <div class="container">
          <div class="catalog-tabs">
            <div class="catalog-tabs-nav">
              <div class="catalog-tabs-nav-title title">Возможно вас заинтересуют эти товары</div>
            </div>
            <div class="catalog-arrows">
              <div class="catalog-arrow catalog-arrow-left">
                <div class="icon icon-arrow-l"></div>
              </div>
              <div class="catalog-arrow catalog-arrow-right">
                <div class="icon icon-arrow-r"></div>
              </div>
            </div>
            <div class="catalog-tabs-list">
              <div class="catalog-tabs-item catalog-tabs-item--1 active">
                <div class="catalog-list catalog-list-4">
                @foreach ($popular as $product)
                  <div class="catalog-item">
                    <div class="catalog-stars">
                      <div class="icon icon-star catalog-star catalog-star--active"></div>
                      <div class="icon icon-star catalog-star catalog-star--active"></div>
                      <div class="icon icon-star catalog-star catalog-star--active"></div>
                      <div class="icon icon-star catalog-star catalog-star--active"></div>
                      <div class="icon icon-star catalog-star catalog-star--active"></div>
                    </div>
                    <div class="catalog-labels">
                    </div>
                    <div class="catalog-image">
                      <a href="/product/{{$product->id}}"><img src="/storage/{{ json_decode($product->images)[0]}}" alt=""></a>
                    </div>
                    <div class="catalog-title">
                      <a href="/product/{{$product->id}}">{{$product->title}}</a>
                    </div>
                    <div class="catalog-price">
                      {{$product->price}} <span>й</span>
                    </div>
                    <div class="catalog-button">
                      <a href="javascript:void(0)"><span>В корзину</span><i class="icon icon-plus"></i></a>
                    </div>
                  </div>
                @endforeach
                </div>
                <br>
                <br>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection