<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PETSHOP - @yield('title')</title>
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#733a1c">
  <meta name="theme-color" content="#ffffff">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123902983-1"></script>
  <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-123902983-1');
  </script>
  <link rel="stylesheet" href="/media/css/build.css?v5" async>
</head>
<body>
<div class="wrap_fix">
  <div class="header">
    <div class="container">
      <div class="header-row">
        <div class="header-logo">
          <a href="/"></a>
        </div>
        <div class="header-mobile">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div class="header-menu">
          <ul class="header-menu-list">
            <li><a href="/">Главная</a></li>
            <li><a href="/products">Корм для собак</a></li>
            <li><a href="/page/delivery">Доставка</a></li>
            <li><a href="/page/pay">Оплата</a></li>
            <li><a href="/page/about">О нас</a></li>
          </ul>
        </div>
        <div class="header-buttons">
          <div class="header-search">
            <form action="/s" method="GET">
              <input type="text" name="key" placeholder="Поиск">
            </form>
          </div>
          <ul class="header-buttons-list">
            <li><a href="/cart" class="cart-link"><span class="icon icon-basket"></span><em>{{Cart::count()}}</em></a></li>
            @if (Auth::user())
            <li><a href="/profile"><span class="icon icon-user"></span></a> <div class="auth-box">
              <span><a href="/profile">{{Auth::user()->name}}</a></span><br>
              <a href="/logout">Выйти</a>
            </div> </li>
            @else
            <li><a href="#login"><span class="icon icon-user"></span></a></li>
            @endif
          </ul>
        </div>
        <div class="header-contacts">
          <div class="header-contacts-phone"><a href="tel:+74993983630">+7 499 398 36 30</a></div>
          <div class="header-contacts-email"><a href="mailto:info@petdiets.ru">info@petdiets.ru</a></div>
          <div class="header-contacts-time">Пн - Пт 10:00-19:00 </div>
        </div>
      </div>
    </div>
  </div>
  <div class="navigation">
    <div class="container">
      <ul class="navigation-list">
        @foreach ($cats as $c)
        <li><a href="/category/{{$c->id}}">{{$c->title}}</a></li>
        @endforeach
        <li><a href="#ingridients" class="scrollto">Ингридиенты</a></li>
        <li><a href="#bottom" class="scrollto">Специальные предложения</a></li>
      </ul>
    </div>
  </div>
</div>
  @yield('content')
  <div class="bottom" id="bottom">
    <div class="container">
      <div class="bottom-title">
        <div class="bottom-title-left">Специальное предложение</div>
      </div>
      <div class="bottom-form">
        <div class="bottom-form-title">
           Введите имя своего питомца<br> и мы вышлем вам подарок на указанный e-mail
        </div>
        <form action="javascript:void(0)" id="subscribe_form" class="bottom-form-item">
          <input type="text" class="bottom-form-item-text" required name="name" placeholder="Имя питомца">
          <input type="text" class="bottom-form-item-text" data-rule-email="true" required name="email" placeholder="E-mail">
          @csrf
          <input type="submit" value="Оставить заявку" class="bottom-form-item-button">
        </form>
      </div>
    </div>
  </div>
  @if (Request::is('/'))
  <div class="footer-text">
    <div class="container">
      <h1>Купить корм для собак в интернет магазине</h1>
      <p>Корм для собаки – основа ее жизнедеятельности и ваша забота о своем питомце. Поэтому, важно обращать внимание на состав, чтобы покупать исключительно полезные и питательные корма, такие как "Petdiets"</p>
      <p>В жизни современного человека собаки играют большую роль. А в некоторых случаях – даже очень большую. Все зависит от того, кто и с какой целью заводит четвероногого друга. Кто-то берет собаку из приюта, чтобы спасти от незавидной участи. Кто-то покупает на выставке щенка от родителей с родословной. А для кого-то собака может быть защитником, охранником, помощником или поводырем. Но всех людей, у которых живут эти милые четвероногие друзья, объединяет один, но очень важный вопрос: какой корм покупать для собак?</p>
      <h2>Нужен ли корм для собак премиум класса?</h2>
      <p>К сожалению, далеко не каждый, у кого дома живет собака, до конца осознает, какое невероятное значение имеет правильное и полноценное питание его питомца. Да, мы согласны с тем, что эта фраза заезжена и банальна. Но из года в год она не теряет свою актуальность, потому что далеко не всякий владелец, приходя в магазин, чтобы купить корм для собак, задается вопросом состава. А не мешало бы. Ведь иной раз даже корм для собак премиум класса, содержит зерновых компонентов больше, чем мясных. </p>
      <p>Казалось бы, а что тут такого? Ведь собачьи корма премиум класса производятся известными компаниями. И если они делают так, значит это правильно. Не совсем точное утверждение. Прежде чем купить собачий корм, покажите состав разным ветеринарам. А еще лучше – собаководам с опытом. Что они скажут? Склоняемся к мнению, что для многих владельцев собак их ответы будут откровением. </p>
      <p>Мы тоже могли бы подробно рассказать, почему тот или иной корм для собак премиум класса можно поставить под сомнение. Но скажем коротко. Зерновые компоненты не приносят пользу вашему питомцу. А если их гораздо больше чем мясных или овощных, то, скорее, даже наоборот. Если вы возьмете собачьи корма премиум класса и посмотрите состав, вы вряд ли увидите там процентную составляющую. Но продукты располагаются в списке по правилу: первым идет тот компонент, которого больше всего; а последним, которого меньше всего. Все остальные выстраиваются в цепочку по мере уменьшения количества. </p>
      <p>Теперь вы понимаете, почему так важно обращать внимание на то, какой именно вы хотите купить корм для собак, вне зависимости от того, кем он производится? Наверное, это уже просто риторический вопрос. Но значит ли это, что такие корма так уж плохи? Давайте разберемся в этом вопросе.</p>
      <h3>Плюсы, минусы, а также – альтернативы кормов для собак</h3>
      <p>Итак, немного разобравшись в вопросе становится понятно, что составы собачьих кормов могут быть далеко не идеальными. Но, наверное, теперь не понятно, почему они востребованы? В первую очередь, и с этим не поспоришь, в силу удобства. Ведь практично открыть пачку и насыпать, сколько надо. Во-вторых, эти корма, как ни крути, имеют немало полезных веществ и микроэлементов, которые необходимы собаке. В-третьих, немало собачников помимо специального предпочитают также купить собачий корм натуральный и чередовать при кормлении. Эти корма, как раз в данном случае и выступают альтернативой.</p>
      <p>Натуральные собачьи корма – это сочетание мясных продуктов и овощей в тех пропорциях, которые обеспечат собаке полноценный и эффективный питательный рацион. Такие корма не содержат зерновых культур, а используемые мясные и овощные ингредиенты проходят строгий санитарный контроль, что гарантирует их свежесть, качество и пользу.</p>
      <p>Кормить собаку натуральными кормами также просто и удобно, как и специальными. Еда для вашего питомца поставляется в виде замороженных тубусов, которые будут готовы к употреблению в пищу, стоит их только разморозить.</p>
      <p>Вы можете купить корм для собак в интернет магазине уже сегодня с доставкой по вашему адресу. Гарантируем, ваш питомец будет в восторге от предложенного корма.</p>
    </div>
  </div>
  @endif

  <div class="footer footer-box">
    <div class="container">
      <div class="footer-row">
        <div class="footer-logo">
        </div>
        <div class="footer-menu">
          <ul>
            <li><a href="/">Главная</a></li>
            <li><a href="/products">Корм для собак</a></li>
            <li><a href="/page/delivery">Доставка</a></li>
            <li><a href="/page/pay">Оплата</a></li>
            <li><a href="/page/about">О нас</a></li>
            <li><a href="/news">Блог</a></li>
            <li><a href="/contacts">Контакты</a></li>
          </ul>
        </div>
        <div class="footer-info">
          <div class="footer-info-title">Адрес:</div>
          <div class="footer-info-text">г. Москва, Мичуринский проспект,  <br>ул. Олимпийская деревня 1к1</div>
          <div class="footer-info-title">Время работы:</div>
          <div class="footer-info-text">Пн - Пт 10:00-19:00</div>
        </div>
        <div class="footer-social">
          <div class="footer-social-link">
            <div class="footer-social-link-title">Присоединяйтесь:</div>
            <div class="footer-social-link-list">
              <ul>
                <li><a href="http://vk.com/public90645054" target="_blank"><span class="icon icon-vk"></span></a></li>
                <li><a href="https://www.instagram.com/petdiets/" target="_blank"><span class="icon icon-insta"></span></a></li>
                <li><a href="https://www.facebook.com/pages/Petdiets/938007716232187" target="_blank"><span class="icon icon-fb"></span></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-contacts">
          <div class="footer-info-title">Контакты:</div>
          <div class="footer-contacts-phone">+7 499 398 36 30</div>
          <div class="footer-contacts-mail"><a href="mailto:info@petdiets.ru">info@petdiets.ru</a></div>
          <br>
          <br>
          <div class="footer-info-title">Реквизиты:</div>
          <div class="footer-info-text">ОГРН 1157746126724, ИНН 7729447311, КПП 772901001</div>
        </div>
      </div>
      <div class="footer-copy">© Petdiets 2018</div>
    </div>
  </div>

  <div class="remodal modal" data-remodal-id="login">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="modal-login">
      <div class="modal-login-left">
        <div class="modal-login-title">Я уже покупал</div>
        <div class="modal-login-form">
          <form action="javascript:void(0)" id="modal_login" method="POST">
            <div class="modal-login-result"></div>
            <div class="modal-login-field">
              <label for="">E-mail</label>
              <input type="text" name="email" required data-msg="Введите ваш e-mail" class="modal-field-text">
            </div>
            <div class="modal-login-field">
              <label for="">Пароль</label>
              <input type="password" name="password" data-msg="Введите ваш пароль" required class="modal-field-text">
            </div>
            <div class="modal-login-buttons">
              <input type="submit" class="modal-field-submit" value="Войти">
            </div>
            @CSRF
          </form>
        </div>
      </div>
      <div class="modal-login-middle">
        или
      </div>
      <div class="modal-login-right">
        <div class="modal-login-right-title">Я новый покупатель</div>
        <div class="modal-login-right-button"><a href="#register">Зарегистрироваться</a></div>
        <div class="modal-login-right-button-2"><a href="/cart">Купить без регистрации <span>быстрый заказ</span></a></div>

      </div>
    </div>

  </div>

  <div class="remodal modal modal--forgot" data-remodal-id="forgot">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="modal-forgot">
      <div class="modal-forgot-title">Забыли пароль?</div>
      <div class="modal-forgot-text">На указанный email будет выслан новый пароль для входа в аккаунт.</div>
      <div class="modal-form">
        <form action="">
          <div class="modal-form-field">
            <input type="text" class="modal-field-text" placeholder="E-mail">
          </div>
          <div class="modal-form-field">
            <input type="submit" class="modal-field-submit" value="OK">
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="remodal modal modal--register" data-remodal-id="register">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="modal-register">
      <div class="modal-register-title">Регистрация</div>
      <div class="modal-register-form">
        <div class="register-result"></div>
        <form action="javascript:void(0)" method="POST" id="registerForm" class="modal-register-form-content">
          <div class="modal-form-field ">
            <label for="">Имя <span>*</span></label>
            <input type="text" class="modal-field-text" data-msg="Введите имя" required name="name" placeholder="Иван">
          </div>
          <div class="modal-form-field">
            <label for="">Электронная почта <span>*</span></label>
            <input type="email" class="modal-field-text" data-msg="Введите email" data-msg-email="Введите корреетный email" data-rule-email="true" required name="email" placeholder="yourname@example.com">
          </div>
          <div class="modal-form-field">
            <label for="">Пароль <span>*</span></label>
            <input type="password" class="modal-field-text" id="password_1" data-rule-minlength="6" data-msg="Введите пароль" required name="password" data-msg-minlength="Пароль слишком короткий" placeholder="Пароль">
          </div>
          <div class="modal-form-field">
            <label for="">Пароль <span>*</span></label>
            <input type="password" class="modal-field-text" id="password_2" data-rule-minlength="6" data-rule-equalTo="#password_1" data-msg="Подтвердите пароль" data-msg-equalTo="Пароли не совпадают" data-msg-minlength="Пароль слишком короткий" required name="password_confirmation" placeholder="Пароль">
          </div>
          <div class="modal-form-field">
            <label for="">Мобильный телефон для звонка курьера <span>*</span></label>
            <input type="text" class="modal-field-text phone-field" data-msg="Введите номер телефона" required name="phone" placeholder="+79031234567">
          </div>
          <div class="modal-form-field">
            <input type="submit" value="Регистрация" class="modal-field-submit">
          </div>
          <div class="modal-form-help"><span>*</span>Поля обязательные для заполнения</div>
          @csrf
        </form>
      </div>
      <div class="modal-register-bottom">
        <br>
        <br>
        <div class="modal-register-bottom-info">
          <i class="icon icon-lock"></i><a href="#login">Войти по логину и паролю</a>, если уже регистрировались ранее.
        </div>
      </div>
    </div>
  </div>

  <script src="/build/build.js" async ></script>
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
  <script>
    ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
        center: [55.67930223325379,37.46964822883604],
        zoom: 16,
        controls: []
    });

      var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {});

      myMap.geoObjects.add(myPlacemark);
      myMap.behaviors.disable('scrollZoom');
      map.behaviors.disable('drag');
  });
  </script>


</body>
</html>