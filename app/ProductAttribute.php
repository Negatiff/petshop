<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ProductAttribute extends Model
{
  public function attribute()
  {
      return $this->belongsTo('App\Attribute', 'attribute_id', 'id');
  }
  public function attribute_val()
  {
      return $this->belongsTo('App\AttributesValue', 'attribute_value', 'id');
  }
  public function product()
  {
      return $this->belongsTo('App\Product', 'product_id', 'id');
  }
}
