<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Category;
use App\Product;
use App\ProductAttribute;
use App\Attribute;
use App\CategoryAttribute;
use App\Page;
use App\Order;
use App\News;
use App\User;
use App\Subscriber;
use App\ShippingCart;
use Auth;
use Cart;

class MainController extends Controller
{
    public function main()
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $new = Product::take(10)->orderBy('created_at','DESC')->get();
      $best = Product::take(10)->where('best',1)->orderBy('created_at','DESC')->get();
      $sale = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      $popular = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      $dogs = Product::take(10)->orderBy('created_at','DESC')->get();
      $lac = Product::where('category_id',15)->orderBy('created_at','DESC')->get();
      $term = Product::where('category_id',17)->orderBy('created_at','DESC')->get();
      $meat = Product::where('category_id',18)->orderBy('created_at','DESC')->get();
      return view('welcome',['cats'=>$cats,'new'=>$new,'best'=>$best,'sale'=>$sale,'popular'=>$popular,'dogs'=>$dogs,'lac'=>$lac,'term'=>$term,'meat'=>$meat]);
    }
    public function category($id, Request $request)
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $cat = Category::find($id);
      $products = Product::where('category_id',$id)->orderBy('created_at','DESC')->paginate(100);
      $filters = CategoryAttribute::where('category_id',$id)->get();
      $products_filters = ProductAttribute::query();
      if (count($request->query) > 0) {
      $attrs_id = [];
      $attrs_val = [];
      foreach ($request->query as $val => $r) {
        $products = $products_filters->orWhere([['attribute_value', $r],['attribute_id',$val]])->get();
        //$attrs_id[] = $r;
        //$attrs_val[] = $val;
      }
      //$products =  $products_filters->whereIn('attribute_value', $attrs_id)->whereIn('attribute_id', $attrs_val)->get();
      return view('category_filter',['cats'=>$cats,'cat'=>$cat,'products'=>$products,'filters'=>$filters]);
    } else {
      return view('category',['cats'=>$cats,'cat'=>$cat,'products'=>$products,'filters'=>$filters]);
    }
    }
    public function products()
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $products = Product::all();
      $filters = Attribute::all();
      $lac = Product::where('category_id',15)->orderBy('created_at','DESC')->get();
      $term = Product::where('category_id',17)->orderBy('created_at','DESC')->get();
      $meat = Product::where('category_id',18)->orderBy('created_at','DESC')->get();
      return view('products',['cats'=>$cats,'products'=>$products,'lac'=>$lac,'term'=>$term,'meat'=>$meat]);
    }
    public function product($id)
    {
      $product = Product::find($id);
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $products = Product::orderBy('created_at','DESC')->paginate(12);
      $popular = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      $attributes = ProductAttribute::where('product_id',$id)->get();
      return view('product',['cats'=>$cats,'product'=>$product,'products'=>$products,'popular'=>$popular,'attributes'=>$attributes]);
    }
    public function delivery()
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $popular = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      return view('delivery',['cats'=>$cats,'popular'=>$popular]);
    }
    public function pay()
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $popular = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      return view('pay',['cats'=>$cats,'popular'=>$popular]);
    }
    public function about()
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $popular = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      return view('about',['cats'=>$cats,'popular'=>$popular]);
    }
    public function cart()
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $popular = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      return view('cart',['cats'=>$cats,'popular'=>$popular]);
    }
    public function cartAdd()
    {
      Cart::add(['id' => $_POST['id'],'name' => $_POST['product'], 'qty' => $_POST['qty'], 'price' => $_POST['price']]);
      return Cart::count();
    }
    public function cartGet()
    {
      $total = 0;
      foreach (Cart::content() as $row) {
        $thisProduct = Product::find($row->id);
        if ($thisProduct->sale == 1) {
          $sale = ($thisProduct->price * $thisProduct->sale_size)/100;
          $total = $total + ceil(($row->price - $sale) * $row->qty);
        } else {
          $total = $total + $row->price * $row->qty;
        }
      }
      return $total;
    }
    public function cartUpdate(Request $request)
    {
      Cart::update($request->rowid, $request->qty);
      return Cart::count();
    }
    public function registerUser(Request $request)
    {
      $user = new User();
      $user->name = $request->name;
      $user->password = Hash::make($request->password);
      $user->email = $request->email;
      $user->phone = $request->phone;
      $user->save();
      return redirect('/#login');
    }
    public function cartRemove(Request $request)
    {
      Cart::remove($request->id);
      return back();
    }
    public function orderCartRemove(Request $request)
    {
      Cart::remove($request->id);
      ShippingCart::where('identifier',$request->order_id)->delete();
      Cart::store($request->order_id);
      return back();
    }
    public function orderCartUpdate(Request $request)
    {
      Cart::update($request->rowid, $request->qty);
      ShippingCart::where('identifier',$request->order_id)->delete();
      Cart::store($request->order_id);
      return back();
    }
    public function orderUpdatePrice(Request $request)
    {
      $order = Order::where('id',$request->id)->first();
      $order->delivery_price = $request->delivery_price; 
      $order->total = $request->total; 
      $order->total_price = $request->total_price; 
      $order->order_sale = $request->order_sale;
      $order->save();
      return back()->with(['message' => "Цены обновлены", 'alert-type' => 'success']);
    }
    public function updateComment(Request $request)
    {
      $order = Order::where('id',$request->id)->first();
      $order->admin_comment = $request->admin_comment; 
      $order->save();
      return back()->with(['message' => "Комментарий обновлен", 'alert-type' => 'success']);
    }
    public function orderCartAdd(Request $request)
    {
      if ($request->qty) {
        if ($product = Product::find($request->product_id)) {
          Cart::add(['id' => $product->id,'name' => $product->title, 'qty' =>$request->qty, 'price' => $product->price]);
          ShippingCart::where('identifier',$request->order_id)->delete();
          Cart::store($request->order_id);
          return back()->with(['message' => "Товар успешно добавлен!", 'alert-type' => 'success']);
        } else {
          return back()->with(['message' => "Товар с таким ID не найден!", 'alert-type' => 'error']);
        }
      } else {
        return back()->with(['message' => "Количество не заполненно!", 'alert-type' => 'error']);
      }
      
    }
    public function logout()
    {
      Auth::logout();
      return redirect('/');
    }
    public function page($url)
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $popular = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      $page = Page::where('url',$url)->first();
      return view('page',['cats'=>$cats,'popular'=>$popular,'page'=>$page]);
    }
    public function ingr_1()
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $popular = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      return view('ingr_1',['cats'=>$cats,'popular'=>$popular]);
    }
    public function ingr_2()
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $popular = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      return view('ingr_2',['cats'=>$cats,'popular'=>$popular]);
    }
    public function news()
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $news = News::orderBy('created_at','DESC')->paginate(8);
      return view('news',['cats'=>$cats,'news'=>$news]);
    }
    public function contacts()
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      return view('contacts',['cats'=>$cats]);
    }
    public function news_item($id)
    {
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $news = News::where('id',$id)->first();
      $popular = Product::take(10)->where('action',1)->orderBy('created_at','DESC')->get();
      return view('news_item',['cats'=>$cats,'news'=>$news,'popular'=>$popular]);
    }
    public function profile()
    {
      if (Auth::user()) {
        $user_id = Auth::user()->id;
        $orders = Order::where('user',$user_id)->get();
        return view('profile',['orders'=>$orders]);
      } else {        
        return view('profile');
      }
    }
    public function checkout(Request $request)
    {      
      $userId = $request->user_id;
      $userEmail = $request->user_email;
      if ($request->user_email == 0) {
        if (count(User::where('email',$request->mail)->get()) > 0) {
          $userCur = User::where('email',$request->mail)->first();
          $userEmail = $request->email;
          $userId = $userCur->id;
        } 
      }
      if (count(User::where('email',$request->mail)->get()) == 0) {
        if ($request->user_id == "0"){
          $user = new User;
          $user->name = $request->name;
          $user->phone = $request->phone;
          $user->email = $request->mail;
          $user->password = Hash::make(str_random(8));
          $user->old = 0;
          $user->save();
          $userId = $user->id;
          $userEmail = $user->email;
        }
      }
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      $order = new Order;
      $order->delivery_type = $request->delivery_method;
      $order->address = $request->address;
      $order->phone = $request->phone;
      $order->name = $request->name;
      $order->email = $userEmail;
      $order->comment = $request->comment;
      $order->user = $userId;
      $order->body = $request->body;
      $order->total = $request->subtotal_sum;
      $order->total_price = $request->total_sum;
      $order->delivery_price = $request->delivery_sum;
      $order->order_id = date('dmy') .'-' .date('His'). $userId;
      $order->save();
      $id = $order->order_id;
      Cart::store($id);
      Cart::destroy();
      return view('checkout',['cats'=>$cats,'order_id'=>$id]);
    }
    public function search(Request $request)
    {
      $error = ['error' => 'Ничего не найдено'];
      $cats = Category::where('parent_id',NULL)->orderBy('order_field','ASC')->get();
      if($request->has('key')) {
          $products = Product::where('title','LIKE', '%' .$request->get('key').'%')->get();
      }
      return view('search',['cats'=>$cats,'products'=>$products]);
    }
    public function invoice($id)
    {
      $order = Order::findOrFail($id);
      return view('order.invoice',['order'=>$order]);
    }
    public function invoice_mass(Request $request)
    {
      $ids = $request->id;
      $ids_array = explode(",", $ids);
      $orders = Order::whereIn('id',$ids_array)->get();
      return view('order.invoices',['orders'=>$orders]);
    }
    public function statusupdate(Request $request)
    {
      $id = $request->id;
      $order = Order::find($id);
      $order->status = $request->status;
      $order->save();
    }
    public function usercomment(Request $request)
    {
      $id = $request->id;
      $user = User::find($id);
      $user->comment = $request->comment;
      $user->save();
    }
    public function list($id)
    {
      $order = Order::findOrFail($id);
      return view('order.list',['order'=>$order]);
    }
    public function list_mass(Request $request)
    {
      $ids = $request->id;
      $ids_array = explode(",", $ids);
      $orders = Order::whereIn('id',$ids_array)->get();
      return view('order.lists',['orders'=>$orders]);
    }
    public function path($id)
    {
      $order = Order::findOrFail($id);
      return view('order.path',['order'=>$order]);
    }
    public function path_mass(Request $request)
    {
      $ids = $request->id;
      $ids_array = explode(",", $ids);
      $orders = Order::whereIn('id',$ids_array)->get();

      return view('order.paths',['orders'=>$orders]);
    }
    public function subscribe(Request $request)
    {
       $sub = new Subscriber;
       $sub->pet = $request->name;
       $sub->email = $request->email;
       $sub->save();
       return 1;
    }
}
