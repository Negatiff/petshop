$(document).ready(function(){
	$('.main-slider').slick({
		dots: true,
		arrows: false
	});

	$("a.scrollto").click(function() {
    var elementClick = $(this).attr("href")
    var destination = $(elementClick).offset().top;
    jQuery("html:not(:animated),body:not(:animated)").animate({
      scrollTop: destination
    }, 800);
    return false;
  });

  $('.wrap_fix').sticky();

	$('.header-mobile').on('click', function(){
		$(this).toggleClass('open');
		$('.main-menu').toggle('fast');
	});

	$('.catalog-button a').on('click', function(){
		$(this).closest('form').submit();
	});
	$('.product-button a').on('click', function(){
		$(this).closest('form').submit();
	});

	$('.js-select').selectric();

	$('.mobile-navigation .catalog-list li').on('click', 'a', function(){
		$(this).parent().toggleClass('open');
		$(this).parent().find('ul').toggleClass('active');
		$(this).parent().find('ul').slick('setPosition'); 
	});

	$('.product-qty-minus').on('click', function(){
		var val = $(this).next().val();
		if (val > 1) {
			val--;
		}
		$(this).next().val(val);
	});
	$('.product-qty-plus').on('click', function(){
		var val = $(this).prev().val();
		val++;
		$(this).prev().val(val);
	});

	$('ul.tabs-nav').on('click', 'li:not(.active)', function() {
		if ($(window).width() < 755) {
			$('.tabs-content .product-list').hide();
			$('.tabs-content .product-list').slick('slickNext');
		}
		$(this)
			.addClass('active').siblings().removeClass('active')
			.closest('div.tabs').find('div.tabs-content').removeClass('active').eq($(this).index()).addClass('active');
		if ($(window).width() < 755) {setTimeout(function(){$('.tabs-content .product-list').fadeIn();},400);}
	});

	$('.category-left .title').on('click', function() {
		$(this).next().toggle('fast');
	});

	$('.select-current').on('click', function() {
		$(this).next().toggle('fast');
	});

	$('.catalog-tabs-nav a').on('click', function() {
		$('.catalog-tabs-nav li').removeClass('active');
		$(this).parent().addClass('active');
		var id = $(this).data('tab');
		$('.catalog-tabs-item').removeClass('active');
		$('.catalog-tabs-item--'+id).addClass('active');
	});

	$('.product-tabs-nav a').on('click', function() {
		$('.product-tabs-nav li').removeClass('active');
		$(this).parent().addClass('active');
		var id = $(this).data('tab');
		$('.product-tabs-item').removeClass('active');
		$('.product-tabs-item--'+id).addClass('active');
	});
	$('.catalog-list-4').slick({
		dots: false,
		arrows: false,
		slidesToShow: 4,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 755,
				settings: {
					slidesToShow: 1
				}
			},
			{
				breakpoint: 560,
				settings: {
					slidesToShow: 1
				}
			},
		]
	});
	$('.catalog-list-3').slick({
		dots: false,
		arrows: true,
		slidesToShow: 3,
		nextArrow: '<div class="slider-arrow-right slider-arrow"><i class="icon icon-arrow-r"></i></div>',
		prevArrow: '<div class="slider-arrow-left slider-arrow"><i class="icon icon-arrow-l"></i></div>',
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 755,
				settings: {
					slidesToShow: 1
				}
			},
			{
				breakpoint: 560,
				settings: {
					slidesToShow: 1,
				}
			},
		]
	});
	$('.catalog-arrow-left').on('click',function(){
		$('.catalog-list-4').slick('slickPrev');
	});
	$('.catalog-button a').on('click',function(){
		$(this).html('<div class="icon icon-check-2"></div>');
		$(this).parent().addClass('yep');
	});
	$('.product-button a').on('click',function(){
		$(this).html('<div class="icon icon-check-2"></div>');
		$(this).parent().addClass('yep');
	});

	$('.catalog-arrow-right').on('click',function(){
		$('.catalog-list-4').slick('slickNext');
	});

	if ($(window).width() < 755) {
		jQuery("meta[name='viewport']").attr('content','width=320');
	}

	if ($(window).width() < 755) {
		$('.product-list-mobile-slider').slick({
			dots: false,
			arrows: true,
			slidesToShow: 2,
			prevArrow: '<div class="slider-arrow-left slider-arrow"><i class="icon icon-arrow-l"></i></div>',
			nextArrow: '<div class="slider-arrow-right slider-arrow"><i class="icon icon-arrow-r"></i></div>',
			responsive: [
				{
					breakpoint: 560,
					settings: {
						slidesToShow: 1,
						nextArrow: '<div class="slider-arrow-right slider-arrow"><i class="icon icon-arrow-r"></i></div>',
						adaptiveHeight: true
					}
				},
			]
		});
	}

	$('.navigation-list-open').slick({
		dots: false,
		arrows: true,
		prevArrow: '<div class="navigation-list-left navigation-list-arrow"><i class="icon icon-arrow-l-white"></i></div>',
		nextArrow: '<div class="navigation-list-right navigation-list-arrow"><i class="icon icon-arrow-r-white"></i></div>'
	});

	$('.catalog-list-open').slick({
		dots: false,
		arrows: true,
		prevArrow: '<div class="navigation-list-left navigation-list-arrow"><i class="icon icon-arrow-l-white"></i></div>',
		nextArrow: '<div class="navigation-list-right navigation-list-arrow"><i class="icon icon-arrow-r-white"></i></div>'
	});

	$('.product-single-image-list').slick({
		dots: false,
		arrows: false,
		slidesToShow: 1,
		asNavFor: '.product-single-image-thumb'
	});


	$('.product-single-image-thumb').slick({
		dots: false,
		arrows: true,
		slidesToShow: 5,
		asNavFor: '.product-single-image-list',
		centerMode: true,
		centerPadding: 0,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 560,
				settings: {
					slidesToShow: 4
				}
			},
		]
	});
	
	if ($(window).width() > 755) {
		$('.reviews-list').baron({
			bar: '.reviews_scroller',
		});
	}

	$('.radio-field').iCheck();
	$('.modal-checkbox').iCheck();
	$('.cart-delivery-radio input').iCheck();
	$('.cart-delivery-checkbox input').iCheck();
	$('.checkbox').iCheck();
	$('.radio-field').on('ifChecked',function(){
		$(this).parent().parent().addClass('label-active');
	});
	$('.radio-field').on('ifUnchecked',function(){
		$(this).parent().parent().removeClass('label-active');
	});
	$('.cart-delivery-checkbox input').on('ifChecked',function(){
		$('.delivery_pet').show('fast');
	});
	$('#delivery-method-2').on('ifChecked',function(){
		$('.delivery-km').show('fast');
		$('.cart-final-sum').show('fast');
		var deliveryprice = $('#delivery_sum').data('price-mkad');
		var km = $('.delivery-km input').val();
		var delivery = km * 20 + parseInt(deliveryprice);
		$('.delivery_price i').text(delivery);
		var total_price = $('.total_price').data('total');
		$('.total_price i').text(total_price + delivery);
		$('#total_sum').val(total_price + delivery);
		$('#delivery_sum').val(delivery);
    $("#cart_km").rules("add", "required");
	});
	$('#delivery-method-2').on('ifUnchecked',function(){
		$('.delivery-km').hide('fast');
		var deliveryprice = $('#delivery_sum').data('price');
		$('.delivery_price i').text(parseInt(deliveryprice));
		var total_price = $('.total_price').data('total');
		$('.total_price i').text(total_price);
    $("#cart_km").rules("remove", "required");
	});
	$('.cart-delivery-checkbox input').on('ifUnchecked',function(){
		$('.delivery_pet').hide('fast');
	});

	$('.cart-remove button').on('click',function(){
		$(this).parent().submit();
		$(this).hide();
	});

	$('.delivery-km input').on('keyup',function(){
		var km = $(this).val();
		var deliveryprice = $('#delivery_sum').data('price-mkad');
		var delivery = km * 20 + parseInt(deliveryprice);
		$('.delivery_price i').text(delivery);
		var total_price = $('.total_price').data('total');
		$('.total_price i').text(total_price + delivery);
		$('#total_sum').val(total_price + delivery);
		$('#delivery_sum').val(delivery);
	});

	$('.filters-title').on('click', function(){
		$(this).toggleClass('open');
		$(this).next().toggleClass('open');
	});

	$('.cart-button-link').on('click', function(){
		$(this).hide();
		$('.cart-delivery-form').show('fast');
		$('.cart-final-sum').show('fast');
		$('.cart-bottom-left').show('fast');
	});

	$('.cart-table .product-qty-plus').on('click', function(){
		var price = $(this).prev().data('price');
		var v = $(this).prev().val();
		$(this).parent().parent().parent().next().find('i').text(price * v);
		var rowid = $(this).data('row');
		var form = $(this).closest('#cart-'+rowid);
		var url = '/cart/update';
	  $.ajax({
	         type: "POST",
	         url: url,
	         data: form.serialize(), // serializes the form's elements.
	         success: function(data)
	         {
	             toastr.success('Корзина обновлена');
	             $('.cart-link em').text(data);
							 $.get( "/cart/get", function( data ) {
							 	 $('#subtotal_sum').val(data);
							   $( ".pretotal_price i" ).text( data );
							 	var delivery_price = $('#delivery_sum').val();
							 	var final_price = parseInt(data)+parseInt(delivery_price);
							   $('.total_price i').text(final_price);
							   $('.total_price').data('total', data).attr('data-total', data);
							   if (data > 1499) {
							   	location.reload();
							   }
							 });
	         }
	       });
	});
	$('.cart-table .product-qty-minus').on('click', function(){
		var price = $(this).next().data('price');
		var v = $(this).next().val();
		$(this).parent().parent().parent().next().find('i').text(price * v);
		$('.pretotal_price').val();
		var rowid = $(this).data('row');
		var form = $(this).closest('#cart-'+rowid);
		var url = '/cart/update';
	  $.ajax({
	         type: "POST",
	         url: url,
	         data: form.serialize(), // serializes the form's elements.
	         success: function(data)
	         {
	             toastr.success('Корзина обновлена');
	             $('.cart-link em').text(data);
							 $.get( "/cart/get", function( data ) {
							 	 $('#subtotal_sum').val(data);
							   $( ".pretotal_price i" ).text( data );
							 	var delivery_price = $('#delivery_sum').val();
							 	var final_price = parseInt(data)+parseInt(delivery_price);
							   $('.total_price i').text(final_price);
							   $('.total_price').data('total', data).attr('data-total', data);
							   if (data < 1500) {
							   	location.reload();
							   }
							   if (data < 5000) {
							   	location.reload();
							   }
							 });
	         }
	       });
	});

	$(".phone-field").mask("+7 (999) 999 99 99");
});


$('.form-cart').on('submit', function(){
	var form = $(this);
	var url = '/cart/add';
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
               toastr.success('Товар добавлен в корзину');
               $('.cart-link em').text(data);
           }
         });
});
$('#modal_login').validate({
	submitHandler: function(form) {
			$('.modal-login-result').hide('fast');
			var url = '/login';
		  $.ajax({
		         type: "POST",
		         url: url,
		         data: $('#modal_login').serialize(), // serializes the form's elements.
		         success: function(data)
		         {
								$('.modal-login-result').addClass('success');
								$('.modal-login-result').text('Авторизация успешна');
								$('.modal-login-result').show('fast');
								document.location.href = '/profile'
		         },
		         error: function(data)
		         {
								$('.modal-login-result').show('fast');
		             $('.modal-login-result').text('Ошибка авторизации');
		         }
		       });
	}
});
$('#registerForm').validate({
	submitHandler: function(form) {
			$('.modal-login-result').hide('fast');
			var url = '/register';
		  $.ajax({
		         type: "POST",
		         url: url,
		         data: $('#registerForm').serialize(), // serializes the form's elements.
		         success: function(data)
		         {
								$('.register-result').addClass('success');
								$('.register-result').text('Регистрация успешна');
								$('.register-result').show('fast');
								document.location.href = '/';
		         },
		         error: function(data)
		         {
								$('.register-result').show('fast');
								var emailMsg = data.responseJSON.errors.email[0];
		             $('.register-result').text(emailMsg);
		         }
		       });
	}
});
$('#subscribe_form').validate({
	submitHandler: function(form) {
			var url = '/subscribe';
		  $.ajax({
		         type: "POST",
		         url: url,
		         data: $('#subscribe_form').serialize(), // serializes the form's elements.
		         success: function(data)
		         {
		         	$("#subscribe_form").remove();
		         	$('.bottom-form-title').text('Спасибо, скоро мы вышлем вам специальное предложение');
		         }
		       });
	}
});

$('#checkoutForm').validate({
	submitHandler: function(form) {
		form.submit();
	}
});
/*
ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [55.58839199312455,37.893485646014696],
            zoom: 16
        }),

        // Создаём макет содержимого.
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        ),

        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'Собственный значок метки',
            balloonContent: 'Это красивая метка'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'media/img/marker.png',
            // Размеры метки.
            iconImageSize: [118, 44],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-59, -22]
        });

    myMap.geoObjects.add(myPlacemark);
});*/